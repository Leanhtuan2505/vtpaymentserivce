﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ConsumerTest.Models;
using Newtonsoft.Json;
using System.IO;

namespace ConsumerTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult ReceivePaymentInfo()
        {
            string request = string.Empty;
            using (var reader = new StreamReader(Request.Body, System.Text.Encoding.UTF8))
            {
                request = reader.ReadToEndAsync().Result;
            }

            var checkout = JsonConvert.DeserializeObject<CheckOutInfoViewModel>(request);

            return Redirect(checkout.CheckoutUrl);
        }
    }
}
