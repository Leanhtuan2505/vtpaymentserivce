﻿using Newtonsoft.Json;

namespace ConsumerTest.Models
{
    public class CheckOutInfoViewModel
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("checkoutUrl")]
        public string CheckoutUrl { get; set; }
    }


}
