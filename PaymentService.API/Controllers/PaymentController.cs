﻿using Microsoft.AspNetCore.Mvc;
using PaymentService.Alepay;
using PaymentService.API.Models;
using PaymentService.MoMo;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaymentService.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : Controller
    {
        private readonly IPaymentStrategy paymentStrategy;
        private readonly RequestService requestService;
        private readonly KeyValuePair<string, string>[] headers = new KeyValuePair<string, string>[]
        {
            new KeyValuePair<string, string>("tv-auth-usr","VT-GAS40"),
            new KeyValuePair<string, string>("tv-auth-pwd","@VT2019-GAS40@"),
            new KeyValuePair<string, string>("tv-api-key","VT-GAS40-2019")
        };

        public PaymentController(IPaymentStrategy paymentStrategy, RequestService requestService)
        {
            this.paymentStrategy = paymentStrategy;
            this.requestService = requestService;
        }

        [HttpGet("Test")]
        public IActionResult GoToTestPage()
        {
            return View("Test", new MoMoPaymentEntity { Amount = "1111100" });
        }

        [HttpPost("MoMo")]
        public async Task<IActionResult> ProcessMoMoPaymentRequest(MoMoPaymentModel model)
        {
            var payment = new MoMoPaymentEntity
            {
                Amount = model.OrderInfo.Amount.ToString(),
                OrderId = model.OrderInfo.OrderId,
                AppBaseUrl = AppBaseUrl,
                ReturnUrl = model.ReturnUrl
            };
            var response = await paymentStrategy.RequestPaymentAsync(payment);

            return Ok(response);
        }

        [HttpGet("MoMo/Status")]
        public async Task<IActionResult> CheckMoMoPayment()
        {
            var accessKey = Request.Query["accessKey"].ToString();
            var orderId = Request.Query["orderId"].ToString();
            var orderType = Request.Query["orderType"].ToString();
            var transId = Request.Query["transId"].ToString();
            var message = Request.Query["message"].ToString();
            var errorCode = Request.Query["errorCode"].ToString();

            var transaction = await paymentStrategy.GetTransactionInfoAsync(new MoMoPaymentEntity { }, orderId);

            // TODO: Send transaction back to consumer 

            return Ok(errorCode);
        }

        [HttpGet("MoMo/Notify")]
        public IActionResult NotifyMoMoPayment()
        {
            var accessKey = Request.Query["accessKey"].ToString();
            var orderId = Request.Query["orderId"].ToString();
            var orderType = Request.Query["orderType"].ToString();
            var transId = Request.Query["transId"].ToString();
            var message = Request.Query["message"].ToString();
            var errorCode = Request.Query["errorCode"].ToString();

            return Ok(errorCode);
        }

        [HttpPost("Alepay")]
        public async Task<IActionResult> ProcessAlepayPaymentRequest(AlepayPaymentRequestModel model)
        {
            var order = model.OrderInfo;
            var payment = new AlepayRequestEntity
            {
                Amount = order.Amount,
                BuyerAddress = order.BuyerAddress,
                BuyerCity = order.BuyerCity,
                BuyerCountry = order.BuyerCountry,
                BuyerEmail = order.BuyerEmail,
                BuyerName = order.BuyerName,
                BuyerPhone = order.BuyerPhone,
                Currency = order.Currency,
                OrderId = order.OrderId,
                OrderDescription = order.OrderDescription,
                PaymentHours = "0",
                TotalItems = order.TotalItems,
                AppBaseUrl = AppBaseUrl,
                ReturnUrl = model.ReturnUrl
            };

            var response = await paymentStrategy.RequestPaymentAsync(payment);

            if (!string.IsNullOrEmpty(model.ReturnUrl))
            {
                await requestService.SendAsync(model.ReturnUrl, response, headers);
                return Ok();
            }

            return Ok(response);
        }

        [HttpGet("Alepay/Result")]
        public async Task<IActionResult> ReceiveAlepayPaymentResult()
        {
            var data = Request.Query["data"].ToString();
            var transaction = await paymentStrategy.GetTransactionInfoAsync(new AlepayRequestEntity { }, data);

            // TODO: Send transaction back to consumer 

            return Ok(transaction);
        }

        private string AppBaseUrl => $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
    }

}