﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using PaymentService.API.Models;

namespace PaymentService.API.Filters
{
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        private ILogger<ApiExceptionFilter> logger;

        public ApiExceptionFilter(ILogger<ApiExceptionFilter> logger)
        {
            this.logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;

            logger.LogError(exception.Message + ":::" + exception.StackTrace);

            var errorInfo = new ErrorInfo();
            errorInfo.Message = exception.Message;
            errorInfo.StatusCode = StatusCodes.Status500InternalServerError;

            context.HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;

            context.Result = new JsonResult(errorInfo);

            base.OnException(context);
        }
    }
}
