﻿using Newtonsoft.Json;

namespace PaymentService.API.Models
{
    public class AlepayPaymentRequestModel : PaymentRequestModel<AlepayOrderInfo>
    { 
    }

    public class AlepayOrderInfo : OrderInfo
    {
        public string OrderId { get ; set ; }

        public decimal Amount { get ; set ; }

        public string Currency { get; set; }

        public string OrderDescription { get; set; }

        public int TotalItems { get; set; }

        public string BuyerName { get; set; }

        public string BuyerEmail { get; set; }

        public string BuyerPhone { get; set; }

        public string BuyerAddress { get; set; }

        public string BuyerCity { get; set; }

        public string BuyerCountry { get; set; }
    }
}
