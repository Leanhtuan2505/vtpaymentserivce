﻿namespace PaymentService.API.Models
{
    public class ErrorInfo
    {
        public string Message { get; internal set; }

        public int StatusCode { get; internal set; }
    }
}
