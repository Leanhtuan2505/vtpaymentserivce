﻿using Newtonsoft.Json;

namespace PaymentService.API.Models
{
    public class MoMoPaymentModel : PaymentRequestModel<MoMoOrderInfo>
    {
    }

    public class MoMoOrderInfo : OrderInfo
    {
        public string OrderId { get; set; }

        public decimal Amount { get; set; }
    }
}
