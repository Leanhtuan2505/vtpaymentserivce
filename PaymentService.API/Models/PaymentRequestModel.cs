﻿using Microsoft.AspNetCore.Mvc;

namespace PaymentService.API.Models
{
    public abstract class PaymentRequestModel<TOrder> where TOrder : OrderInfo
    {
        [BindProperty(Name = "version")]
        public string Version { get; set; }

        [BindProperty(Name = "client")]
        public string Client { get; set; }

        [BindProperty(Name = "clientVersion")]
        public string ClientVersion { get; set; }

        [BindProperty(Name = "sessionID")]
        public string SessionId { get; set; }

        [BindProperty(Name = "returnUrl")]
        public string ReturnUrl { get; set; }

        [BindProperty(Name = "orderInfo")]
        public TOrder OrderInfo { get; set; }

    }

    public interface OrderInfo
    {
        string OrderId { get; set; }

        decimal Amount { get; set; }
    }
}
