using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using PaymentService.Alepay;
using PaymentService.API.Filters;
using PaymentService.MoMo;
using PaymentService.Services;

namespace PaymentService.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<MoMoConfiguration>(Configuration.GetSection("MoMoSettings"));
            services.Configure<AlepayConfiguration>(Configuration.GetSection("AlepaySettings"));

            services.AddMvc(options =>
            {
                options.Filters.Add<ApiExceptionFilter>();
            });
            services.AddControllers();

            services.AddEntityFrameworkSqlServer();
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                options.EnableSensitiveDataLogging(true);
            });

            services.AddTransient<RequestService>();
            services.AddScoped<MoMoPaymentService>();
            services.AddScoped<AlepayPaymentService>();

            var provider = services.BuildServiceProvider();
            services.AddScoped<IPaymentStrategy>(x => new PaymentStrategy(
                provider.GetService<MoMoPaymentService>(),
                provider.GetService<AlepayPaymentService>()
                )
            );
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "VT Payment", Version = "0.1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Logs/vt-payment-{Date}.txt");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", $"MyRegal API v0.1");
            });
        }
    }
}
