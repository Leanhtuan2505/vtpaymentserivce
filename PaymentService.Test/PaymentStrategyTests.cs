using PaymentService.Alepay;
using PaymentService.MoMo;
using PaymentService.Services;
using PaymentService.Strategies;
using Xunit;

namespace PaymentService.Test
{
    public class PaymentStrategyTests
    {
        [Fact]
        public void Test1()
        {
            var paymentStrategy = new PaymentStrategy(new IPaymentService[]    {
                new MoMoPaymentService(new MoMoConfiguration()),
                new AlepayPaymentService(new AlepayConfiguration())
            });


            // Then once it is injected, you simply do this...
            var cc = new MoMoPaymentEntity()
            {
                Amount = "21500"
                /* Set other properties... */
            };
            paymentStrategy.MakePayment(cc);

            // Or this...
            var pp = new AlepayPaymentEntity()
            {
                amount = "125000"
                /* Set other properties... */
            };

            paymentStrategy.MakePayment(pp);
        }
    }
}
