﻿using Newtonsoft.Json;
using PaymentService.Entities;

namespace PaymentService.Alepay
{
    public class AlepayCheckout: ICheckout
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("checkoutUrl")]
        public string CheckoutUrl { get; set; }
    }
}
