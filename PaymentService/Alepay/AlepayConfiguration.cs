﻿using PaymentService.Entities;

namespace PaymentService.Alepay
{
    public class AlepayConfiguration: IPaymentConfiguration
    {
        public string BaseUrl { get; set; }

        public string ReturnUrl { get; set; }

        public string CancelUrl { get; set; }

        public string TokenKey { get; set; }

        public string ChecksumKey { get; set; }

        public string EncryptKey { get; set; }
    }
}
