﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PaymentService.Entities;
using PaymentService.Services;
using PaymentService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PaymentService.Alepay
{
    public class AlepayPaymentService : PaymentService<AlepayRequestEntity>
    {
        protected readonly AlepayConfiguration configuration;
        private readonly JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new List<JsonConverter> { new TickDateTimeConverter() }
        };

        public AlepayPaymentService(ApplicationDbContext dbContext, IOptions<AlepayConfiguration> options) : base(dbContext)
        {
            requestPaymentApi = "/checkout/v1/request-order";
            queryTransactionApi = "/checkout/v1/get-transaction-info";

            configuration = options.Value;
        }

        protected override PaymentRequest ConvertToPaymentRequest(AlepayRequestEntity model)
        {
            return new PaymentRequest
            {
                OrderId = model.OrderId,
                Amount = model.Amount,
                ReturnUrl = model.ReturnUrl,
                TotalItems = model.TotalItems,
                PaymentMethod = PaymentMethod.Alepay,
                Provider = Provider.Alepay,
                RequestStatus = RequestStatus.New
            };
        }

        protected override async Task<IPaymentResponse> SendRequestAsync(object payment)
        {
            var crypto = new AlepayCrypto(configuration);
            var ecryptedData = crypto.Encrypt(payment);

            var checksumData = BuildChecksumData(ecryptedData + configuration.ChecksumKey);
            AlepayRequestData requestData = new AlepayRequestData();
            requestData.Token = configuration.TokenKey;
            requestData.Data = ecryptedData;
            requestData.Checksum = checksumData;

            var serializedData = JsonConvert.SerializeObject(requestData, settings);
            var requestService = new RequestService();
            return await requestService.PostAsync<AlepayResponse>(configuration.BaseUrl + requestPaymentApi, serializedData);
        }

        private static string BuildChecksumData(string input)
        {
            byte[] asciiBytes = Encoding.ASCII.GetBytes(input);
            byte[] hashedBytes = MD5.Create().ComputeHash(asciiBytes);
            string hashedString = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            return hashedString;
        }

        public override async Task<IPaymentResponse> GetTransactionInfoAsync(string data)
        {
            var bytes = Convert.FromBase64String(data);
            var decodedString = Encoding.UTF8.GetString(bytes);

            var crypto = new AlepayCrypto(configuration);
            var checkoutResponse = crypto.Decrypt<AleypayCheckoutResponse>(decodedString);

            var model = new AlepayQueryTransaction { TransactionCode = checkoutResponse.Data };
            var requestInfo = new RequestInfo
            {
                RequestUrl = queryTransactionApi,
                RequestData = JsonConvert.SerializeObject(model)
            };
            dbContext.Add(requestInfo);
            dbContext.SaveChanges();

            var response = await SendRequestToAlepayAsync(model, queryTransactionApi);

            requestInfo.Closed = true;
            if (response.HasError())
            {
                requestInfo.ResponseData = JsonConvert.SerializeObject(response);
                dbContext.SaveChanges();
                throw new ApplicationException(response.ErrorMessage);
            }

            var transaction = crypto.Decrypt<AlepayTransaction>(response.Data);
            requestInfo.OrderId = transaction.OrderId;
            requestInfo.ResponseData = JsonConvert.SerializeObject(transaction);

            var orderRequest = dbContext.PaymentRequests.First(x => x.OrderId == requestInfo.OrderId);
            orderRequest.RequestStatus = RequestStatus.Completed;
            dbContext.SaveChanges();

            return transaction;
        }

        private async Task<AlepayResponse> SendRequestToAlepayAsync(AlepayQueryTransaction model, string url)
        {
            var crypto = new AlepayCrypto(configuration);
            var ecryptedData = crypto.Encrypt(model);

            var checksumData = BuildChecksumData(ecryptedData + configuration.ChecksumKey);
            AlepayRequestData requestData = new AlepayRequestData();
            requestData.Token = configuration.TokenKey;
            requestData.Data = ecryptedData;
            requestData.Checksum = checksumData;

            var serializedData = JsonConvert.SerializeObject(requestData, settings);
            var requestService = new RequestService();
            return await requestService.PostAsync<AlepayResponse>(configuration.BaseUrl + url, serializedData);
        }

        protected override void UpdateRequest(AlepayRequestEntity model)
        {
            model.Refine(configuration);
        }

        protected override ICheckout ConvertToCheckout(IPaymentResponse model)
        {
            var crypto = new AlepayCrypto(configuration);
            return crypto.Decrypt<AlepayCheckout>(((AlepayResponse)model).Data);
        }
    }
}
