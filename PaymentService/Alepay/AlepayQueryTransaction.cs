﻿using Newtonsoft.Json;

namespace PaymentService.Alepay
{
    public class AlepayQueryTransaction 
    {
        [JsonProperty("transactionCode")]
        public string TransactionCode { get; set; }       
    }
}
