﻿using Newtonsoft.Json;

namespace PaymentService.Alepay
{
    public class AlepayRequestData
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("checksum")]
        public string Checksum { get; set; }
    }
}
