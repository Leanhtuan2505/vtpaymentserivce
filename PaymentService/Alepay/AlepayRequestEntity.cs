﻿using Newtonsoft.Json;
using PaymentService.Entities;

namespace PaymentService.Alepay
{
    public class AlepayRequestEntity : IPaymentRequestEntity
    {
        [JsonIgnore]
        public string AppBaseUrl { get; set; }

        [JsonProperty("returnUrl")]
        public string ReturnUrl { get; set; }

        [JsonProperty("cancelUrl")]
        public string CancelUrl { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("orderCode")]
        public string OrderId { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("orderDescription")]
        public string OrderDescription { get; set; }

        [JsonProperty("totalItem")]
        public int TotalItems { get; set; }

        [JsonProperty("checkoutType")]
        public int CheckoutType => 1; // acceptable values: (1: thanh toan thuong, 2: tra gop, 0: ca 2)

        [JsonProperty("buyerName")]
        public string BuyerName { get; set; }

        [JsonProperty("buyerEmail")]
        public string BuyerEmail { get; set; }

        [JsonProperty("buyerPhone")]
        public string BuyerPhone { get; set; }

        [JsonProperty("buyerAddress")]
        public string BuyerAddress { get; set; }

        [JsonProperty("buyerCity")]
        public string BuyerCity { get; set; }

        [JsonProperty("buyerCountry")]
        public string BuyerCountry { get; set; }

        [JsonProperty("paymentHours")]
        public string PaymentHours { get; set; }

        public bool IsValid()
        {
            return !(
                string.IsNullOrEmpty(OrderId) ||
                string.IsNullOrEmpty(OrderDescription) ||
                 Amount <= 0 ||
                string.IsNullOrEmpty(Currency) ||
                TotalItems == 0 ||
                CheckoutType == 0 ||
                string.IsNullOrEmpty(BuyerName) ||
                string.IsNullOrEmpty(PaymentHours) ||
                string.IsNullOrEmpty(BuyerAddress) ||
                string.IsNullOrEmpty(BuyerCity) ||
                string.IsNullOrEmpty(BuyerCountry) ||
                (string.IsNullOrEmpty(BuyerEmail) && string.IsNullOrEmpty(BuyerPhone))
                );
        }

        public void Refine(IPaymentConfiguration configuration)
        {
            var alepayConfiguration = (AlepayConfiguration)configuration;
            ReturnUrl = AppBaseUrl + alepayConfiguration.ReturnUrl;
            CancelUrl = AppBaseUrl + alepayConfiguration.CancelUrl;
        }

    }
}
