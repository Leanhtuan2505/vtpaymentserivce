﻿using Newtonsoft.Json;
using PaymentService.Entities;
using System;

namespace PaymentService.Alepay
{
    public class AlepayResponse : IPaymentResponse
    {
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string ErrorMessage { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("checksum")]
        public string Checksum { get; set; }

        public bool HasError()
        {
            return ErrorCode != "000";
        }
    }
}