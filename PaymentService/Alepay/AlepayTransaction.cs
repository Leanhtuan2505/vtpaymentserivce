﻿using Newtonsoft.Json;
using PaymentService.Entities;

namespace PaymentService.Alepay
{
    public class AlepayTransaction : IPaymentResponse
    {
        [JsonProperty("transactionCode")]
        public string TransactionCode { get; set; }

        [JsonProperty("orderCode")]
        public string OrderId { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("buyerEmail")]
        public string BuyerEmail { get; set; }

        [JsonProperty("buyerPhone")]
        public string BuyerPhone { get; set; }

        [JsonProperty("cardNumber")]
        public string CardNumber { get; set; }

        [JsonProperty("buyerName")]
        public string BuyerName { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("installment")]
        public string Installment { get; set; }

        [JsonProperty("is3D")]
        public string Is3D { get; set; }

        [JsonProperty("month")]
        public string Month { get; set; }

        [JsonProperty("bankCode")]
        public string BankCode { get; set; }

        [JsonProperty("bankName")]
        public string BankName { get; set; }

        [JsonProperty("method")]
        public string Method { get; set; }

        [JsonProperty("transactionTime")]
        public string TransactionTime { get; set; }

        [JsonProperty("successTime")]
        public string SuccessTime { get; set; }

        [JsonProperty("bankHotline")]
        public string BankHotline { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        public bool HasError()
        {
            return ErrorCode != "000";
        }
    }
}
