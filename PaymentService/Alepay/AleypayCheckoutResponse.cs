﻿using Newtonsoft.Json;

namespace PaymentService.Alepay
{
    public class AleypayCheckoutResponse
    {
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("cancel")]
        public string Cancel { get; set; }
    }
}
