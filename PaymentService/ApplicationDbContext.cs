﻿using Microsoft.EntityFrameworkCore;
using PaymentService.Entities;
using System;

namespace PaymentService
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> dbOptions) : base(dbOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            UpdateTrackingInfo();
            return base.SaveChanges();
        }

        private void UpdateTrackingInfo()
        {
            var user = "VietTech";
            var now = DateTime.UtcNow;

            var entries = ChangeTracker.Entries();
            foreach (var entry in entries)
            {
                if (entry.Entity is ITrackable trackable)
                {
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            trackable.LastUpdated = now;
                            trackable.LastUpdatedBy = user;
                            break;

                        case EntityState.Added:
                            trackable.Created = now;
                            trackable.CreatedBy = user;
                            trackable.LastUpdated = now;
                            trackable.LastUpdatedBy = user;
                            break;
                    }
                }
            }
        }

        public DbSet<PaymentRequest> PaymentRequests { get; set; }
        public DbSet<RequestInfo> RequestInfos { get; set; }
    }
}