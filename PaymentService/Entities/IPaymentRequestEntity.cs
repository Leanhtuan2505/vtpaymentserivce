﻿
using PaymentService.Entities;

namespace PaymentService
{
    public interface IPaymentRequestEntity
    {
        string OrderId { get; set; }

        bool IsValid();

        void Refine(IPaymentConfiguration configuration);
    }
}
