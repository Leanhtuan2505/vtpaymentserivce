﻿namespace PaymentService.Entities
{
    public interface IPaymentResponse
    {        
        string ErrorCode { get; set; }

        string ErrorMessage { get; set; }

        bool HasError();
    }

    public interface ICheckout
    {
        string CheckoutUrl { get; set; }
    }

}
