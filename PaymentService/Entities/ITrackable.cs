﻿using System;

namespace PaymentService.Entities
{
    public interface ITrackable
    {
        DateTime LastUpdated { get; set; }

        string LastUpdatedBy { get; set; }

        string CreatedBy { get; set; }

        DateTime Created { get; set; }
    }
}
