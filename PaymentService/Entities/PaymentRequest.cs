﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentService.Entities
{
    [Table("log_order_payment")]
    public class PaymentRequest :  ITrackable
    {
        [Key]
        [Column("order_id")]
        public string OrderId { get; set; }

        [Column("amount")]
        public decimal Amount { get; set; }

        [Column("total_item")]
        public int TotalItems { get; set; }

        [Column("return_url")]
        public string ReturnUrl { get; set; }

        [Column("method")]
        public char Method { get; set; }

        [Column("provider")]
        public string ProviderName { get; set; } // ALEPAY or MOMO

        [Column("status")]
        public char Status { get; set; }

        [Column("update_time")]
        public DateTime LastUpdated { get; set; }    

        [Column("update_by")]
        public string LastUpdatedBy { get; set; }

        [Column("create_by")]
        public string CreatedBy { get; set; }
     
        [Column("create_time")]
        public DateTime Created { get; set; }
     
        public bool IsValid()
        {
            return !string.IsNullOrEmpty(OrderId) && Amount > 0 && TotalItems > 0;
        }

        [NotMapped]
        public PaymentMethod PaymentMethod
        {
            get
            {
                return (PaymentMethod)Method;
            }
            set
            {
                Method = (char)value;
            }
        }

        [NotMapped]
        public RequestStatus RequestStatus
        {
            get
            {
                return (RequestStatus)Status;
            }
            set
            {
                Status = (char)value;
            }
        }

        [NotMapped]
        public Provider Provider
        {
            get
            {
                return Enum.Parse<Provider>(ProviderName);
            }
            set
            {
                ProviderName = value.ToString();
            }
        }     
    }

    public enum PaymentMethod // 1 - Tiền Mặt, 2- Alepay, 3 - Momo
    {
        Unknown = '0',
        Cash = '1',
        Alepay = '2',
        MoMo = '3'
    }

    public enum RequestStatus // N - Just send request, I - Inprocess, C - Completed
    {
        Unknown = '0',
        New = 'N',
        Inprocess = 'I',
        Completed = 'C'
    }

    public enum Provider
    {
        Unknown, MoMo, Alepay
    }
}
