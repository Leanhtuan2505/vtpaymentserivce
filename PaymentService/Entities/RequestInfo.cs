﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentService.Entities
{
    [Table("log_request_service")]
    public class RequestInfo : ITrackable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("request_id")]
        public int RequestId { get; set; }

        [Column("order_id")]
        public string OrderId { get; set; }

        [Column("request_url")]
        public string RequestUrl { get; set; }

        [Column("request_data")]
        public string RequestData { get; set; }

        [Column("response_data")]
        public string ResponseData { get; set; }

        [Column("closed")]
        public bool Closed { get; set; }

        [Column("update_time")]
        public DateTime LastUpdated { get; set; }

        [Column("update_by")]
        public string LastUpdatedBy { get; set; }

        [Column("create_by")]
        public string CreatedBy { get; set; }

        [Column("create_time")]
        public DateTime Created { get; set; }
    }
}
