﻿using System.Collections.Generic;

namespace PaymentService.Helpers
{
    public static class Constants
    {
        internal static readonly string Authorization = "Authorization";
        internal static readonly string Accept = "Accept";
        public static readonly string ApplicationJson = "application/json";
    }
}
