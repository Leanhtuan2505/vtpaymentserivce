﻿using PaymentService.Entities;
using System;
using System.Threading.Tasks;

namespace PaymentService
{
    public interface IPaymentService
    {
        bool AppliesTo(Type provider);

        Task<ICheckout> RequestPaymentAsync<T>(T paymentRequest) where T : IPaymentRequestEntity;

        Task<IPaymentResponse> GetTransactionInfoAsync(string data);
    }
}
