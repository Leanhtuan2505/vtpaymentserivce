﻿using PaymentService.Entities;
using PaymentService.MoMo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentService
{
    public interface IPaymentStrategy
    {
        Task<ICheckout> RequestPaymentAsync<T>(T payment) where T : IPaymentRequestEntity;

        Task<IPaymentResponse> GetTransactionInfoAsync<T>(T payment, string data) where T : IPaymentRequestEntity;
    }

    public class PaymentStrategy : IPaymentStrategy
    {
        private IEnumerable<IPaymentService> paymentServices;

        public PaymentStrategy(params IPaymentService[] paymentServices)
        {
            if (paymentServices == null)
            {
                throw new ArgumentNullException(nameof(paymentServices));
            }

            this.paymentServices = paymentServices;
        }

        public async Task<IPaymentResponse> GetTransactionInfoAsync<T>(T payment, string data) where T : IPaymentRequestEntity
        {
            // TODO: Update order status inside GetTransactionInfo
            return await GetPaymentService(payment).GetTransactionInfoAsync(data);
        }

        public async Task<ICheckout> RequestPaymentAsync<T>(T payment) where T : IPaymentRequestEntity
        {
            return await GetPaymentService(payment).RequestPaymentAsync(payment);
        }

        private IPaymentService GetPaymentService<T>(T model) where T : IPaymentRequestEntity
        {
            var result = paymentServices.FirstOrDefault(p => p.AppliesTo(model.GetType()));
            if (result == null)
            {
                throw new InvalidOperationException($"Payment service for {model.GetType()} not registered.");
            }
            return result;
        }
    }
}
