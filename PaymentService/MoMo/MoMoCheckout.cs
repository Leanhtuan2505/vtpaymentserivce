﻿using Newtonsoft.Json;
using PaymentService.Entities;
using System;

namespace PaymentService.MoMo
{
    public class MoMoCheckout : ICheckout
    {
        [JsonProperty("payUrl")]
        public string CheckoutUrl { get; set; }

        internal static MoMoCheckout From(MoMoResponse response)
        {
            return new MoMoCheckout
            {
                CheckoutUrl = response.PayUrl
            };
        }
    }
}
