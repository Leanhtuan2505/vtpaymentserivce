﻿using PaymentService.Entities;

namespace PaymentService.MoMo
{
    public class MoMoConfiguration : IPaymentConfiguration
    {
        public string PartnerCode { get; set; }

        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string PublicKey { get; set; }

        public string BaseUrl { get; set; }

        public string ReturnUrl { get; set; }

        public string NotifyUrl { get; set; }
    }
}
