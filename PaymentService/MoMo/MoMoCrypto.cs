﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace PaymentService.MoMo
{
    public class MoMoCrypto
    {
        private readonly MoMoConfiguration configuration;

        public MoMoCrypto(MoMoConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public string SignSHA256(string message)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(configuration.SecretKey);
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyBytes))
            {
                byte[] hashBytes = hmacsha256.ComputeHash(messageBytes);
                string hex = BitConverter.ToString(hashBytes);
                hex = hex.Replace("-", "").ToLower();

                return hex;
            }
        }

        public string EncryptWithPublicKey(string json)
        {
            byte[] data = Encoding.UTF8.GetBytes(json);
            string result = null;
            using (var rsa = new RSACryptoServiceProvider(2048))
            {
                try
                {
                    rsa.FromXmlString(configuration.PublicKey);
                    var encryptedData = rsa.Encrypt(data, false);
                    var base64Encrypted = Convert.ToBase64String(encryptedData);
                    result = base64Encrypted;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }

            return result;
        }
    }
}
