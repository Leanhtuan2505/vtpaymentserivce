﻿using Newtonsoft.Json;

namespace PaymentService.MoMo
{
    public class MoMoExceptionDetail
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
