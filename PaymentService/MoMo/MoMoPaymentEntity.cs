﻿using Newtonsoft.Json;
using PaymentService.Entities;
using System;
using System.Linq;
using System.Reflection;

namespace PaymentService.MoMo
{
    public class MoMoPaymentEntity : IPaymentRequestEntity
    {
        public MoMoPaymentEntity()
        {
        }

        [JsonProperty("partnerCode")]
        public string PartnerCode { get; set; }

        [JsonProperty("accessKey")]
        public string AccessKey { get; set; }

        [JsonProperty("requestId")]
        public string RequestId { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("orderId")]
        public string OrderId { get; set; }

        [JsonProperty("orderInfo")]
        public string OrderInfo { get; set; }

        [JsonProperty("returnUrl")]
        public string ReturnUrl { get; set; }

        [JsonProperty("notifyUrl")]
        public string NotifyUrl { get; set; }

        [JsonProperty("extraData")]
        public string ExtraData { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; private set; }

        [JsonProperty("requestType")]
        public string RequestType => "captureMoMoWallet";

        public string AppBaseUrl { get; set; }

        public bool IsValid()
        {
            return !(string.IsNullOrEmpty(OrderId) || string.IsNullOrEmpty(Amount));
        }

        public void Refine(IPaymentConfiguration  configuration)
        {
            var momoConfiguration = (MoMoConfiguration)configuration;
            PartnerCode = momoConfiguration.PartnerCode;
            AccessKey = momoConfiguration.AccessKey;
            RequestId = Guid.NewGuid().ToString();
            OrderInfo = Guid.NewGuid().ToString();
            ReturnUrl = AppBaseUrl + momoConfiguration.ReturnUrl;
            NotifyUrl = AppBaseUrl + momoConfiguration.NotifyUrl;
            ExtraData = string.Empty;
            Signature = new MoMoCrypto(momoConfiguration).SignSHA256(ToRawHash());
        }

        private string ToRawHash()
        {
            var ignoreNames = new string[] {
                nameof(Signature), nameof(RequestType), nameof(AppBaseUrl)
            };
            var properties = GetType().GetProperties().Where(x => !ignoreNames.Contains(x.Name));
            var hashes = properties.Select(p => $"{p.GetCustomAttribute<JsonPropertyAttribute>().PropertyName}={p.GetValue(this)}");

            return string.Join('&', hashes);
        }
    }
}
