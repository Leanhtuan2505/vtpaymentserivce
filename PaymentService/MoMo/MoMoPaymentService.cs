﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PaymentService.Entities;
using PaymentService.Services;
using System;
using System.Threading.Tasks;

namespace PaymentService.MoMo
{
    public class MoMoPaymentService : PaymentService<MoMoPaymentEntity>
    {
        private readonly MoMoConfiguration configuration;

        public MoMoPaymentService(ApplicationDbContext dbContext, IOptions<MoMoConfiguration> options) : base(dbContext)
        {
            configuration = options.Value;
            requestPaymentApi = "/gw_payment/transactionProcessor";
            queryTransactionApi = "/pay/query-status";
        }

        protected override async Task<IPaymentResponse> SendRequestAsync(object payment)
        {
            var request = new RequestService();
            var response = await request.PostAsync<MoMoResponse>($"{configuration.BaseUrl}{ requestPaymentApi}", JsonConvert.SerializeObject(payment));

            return response;
        }

        protected override PaymentRequest ConvertToPaymentRequest(MoMoPaymentEntity model)
        {
            return new PaymentRequest
            {
                OrderId = model.OrderId,
                Amount = Convert.ToDecimal(model.Amount),
                ReturnUrl = model.ReturnUrl,
                TotalItems = 1,
                PaymentMethod = PaymentMethod.MoMo,
                Provider = Provider.MoMo,
                RequestStatus = RequestStatus.New
            };
        }

        protected override void UpdateRequest(MoMoPaymentEntity model)
        {
            model.Refine(configuration);
        }

        protected override ICheckout ConvertToCheckout(IPaymentResponse response)
        {
            return MoMoCheckout.From((MoMoResponse)response);
        }

        public override async Task<IPaymentResponse> GetTransactionInfoAsync(string orderId)
        {
            var request = new RequestService();
            var queryTransactionInfo = new MoMoTransactionEntity
            {
                OrderId = orderId
            };
            queryTransactionInfo.Refine(configuration);
            var response = await request.PostAsync<MoMoResponse>($"{configuration.BaseUrl}{ queryTransactionApi }", JsonConvert.SerializeObject(queryTransactionInfo));

            return response;
        }
    }
}
