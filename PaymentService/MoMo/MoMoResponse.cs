﻿using Newtonsoft.Json;
using PaymentService.Entities;

namespace PaymentService.MoMo
{
    public class MoMoResponse : IPaymentResponse
    {
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("localMessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty("details")]
        public MoMoExceptionDetail[] ErrorDetails { get; set; }

        [JsonProperty("payUrl")]
        public string PayUrl { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }

        public bool HasError()
        {
            return ErrorCode != "0";
        }
    }
}
