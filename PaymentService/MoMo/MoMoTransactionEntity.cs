﻿using Newtonsoft.Json;
using PaymentService.Entities;
using System;

namespace PaymentService.MoMo
{
    public class MoMoTransactionEntity : IPaymentRequestEntity
    {
        [JsonProperty("partnerCode")]
        public string PartnerCode { get; set; }

        [JsonProperty("partnerRefId")]
        public string OrderId { get; set; }

        [JsonProperty("version")]
        public int? Version { get; private set; }

        [JsonProperty("hash")]
        public string Hash { get; private set; }

        [JsonProperty("requestId")]
        public string RequestId { get; set; }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(OrderId);
        }

        public void Refine(IPaymentConfiguration configuration)
        {
            var momoConfiguration = (MoMoConfiguration)configuration;
            PartnerCode = momoConfiguration.PartnerCode;
            RequestId = Guid.NewGuid().ToString();
            var serializedObject = JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            Hash = new MoMoCrypto(momoConfiguration).EncryptWithPublicKey(serializedObject);
            Version = 2;
        }
    }
}