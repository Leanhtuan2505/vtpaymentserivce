﻿using Newtonsoft.Json;
using PaymentService.Entities;
using System;
using System.Threading.Tasks;

namespace PaymentService.Services
{
    public abstract class PaymentService<TModel> : IPaymentService
      where TModel : IPaymentRequestEntity
    {
        protected readonly ApplicationDbContext dbContext;
        protected string requestPaymentApi;
        protected string queryTransactionApi;

        public PaymentService(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public virtual bool AppliesTo(Type provider)
        {
            return typeof(TModel).Equals(provider);
        }

        public async Task<ICheckout> RequestPaymentAsync<T>(T paymentRequestEntity) where T : IPaymentRequestEntity
        {
            var model = (TModel)(object)paymentRequestEntity;
            if (!model.IsValid())
            {
                throw new ApplicationException("Invalid payment information.");
            }
            PaymentRequest paymentRequest = ConvertToPaymentRequest(model);
            dbContext.Add(paymentRequest);
            dbContext.SaveChanges();

            UpdateRequest(model);
            var requestInfo = new RequestInfo
            {
                OrderId = model.OrderId,
                RequestUrl = requestPaymentApi,
                RequestData = JsonConvert.SerializeObject(model)
            };
            dbContext.Add(requestInfo);
            dbContext.SaveChanges();

            var response = await SendRequestAsync(model);
            requestInfo.Closed = true;

            if (response.HasError())
            {
                requestInfo.ResponseData = JsonConvert.SerializeObject(response);
                dbContext.SaveChanges();

                throw new ApplicationException(response.ErrorMessage);
            }

            var checkout = ConvertToCheckout(response);
            requestInfo.ResponseData = JsonConvert.SerializeObject(checkout);
            dbContext.SaveChanges();

            return checkout;
        }

        protected abstract void UpdateRequest(TModel model);

        protected abstract PaymentRequest ConvertToPaymentRequest(TModel model);

        protected abstract ICheckout ConvertToCheckout(IPaymentResponse model);

        protected abstract Task<IPaymentResponse> SendRequestAsync(object payment);

        public abstract Task<IPaymentResponse> GetTransactionInfoAsync(string data);
    }
}
