﻿using Newtonsoft.Json;
using PaymentService.Entities;
using PaymentService.Helpers;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace PaymentService
{
    public class RequestService
    {
        public async Task<T> PostAsync<T>(string url, string serializedData) where T : IPaymentResponse
        {
            using var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.ApplicationJson));
            var responseMessage = await httpClient.PostAsync(url, new StringContent(serializedData, Encoding.UTF8, Constants.ApplicationJson));
            var responseString = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(responseString);
        }

        public async Task SendAsync(string url, object data, KeyValuePair<string, string>[] headers)
        {
            var serializedData = JsonConvert.SerializeObject(data);
            using var httpClient = new HttpClient();

            if (headers != null)
            {
                Array.ForEach(headers, x => httpClient.DefaultRequestHeaders.Add(x.Key, x.Value));
            }

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Constants.ApplicationJson));
            await httpClient.PostAsync(url, new StringContent(serializedData, Encoding.UTF8, Constants.ApplicationJson));
        }
    }
}
